package br.com.mastertech.oAuthEx01.services;

import br.com.mastertech.oAuthEx01.model.Contato;
import br.com.mastertech.oAuthEx01.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criarContato(Contato contato)
    {
        contato.setId(0);
        return contatoRepository.save(contato);
    }

    public List<Contato> buscarContatos(int donoId)
    {
        return contatoRepository.findByDono(donoId);
    }
}
