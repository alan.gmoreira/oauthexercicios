package br.com.mastertech.oAuthEx01.model.dto;

public class RequisicaoNovoContato {
    private String nome;
    private String telefone;

    public RequisicaoNovoContato() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
