package br.com.mastertech.oAuthEx01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OAuthEx01Application {

	public static void main(String[] args) {
		SpringApplication.run(OAuthEx01Application.class, args);
	}

}
