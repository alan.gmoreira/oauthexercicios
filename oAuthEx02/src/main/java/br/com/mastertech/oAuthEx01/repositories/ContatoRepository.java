package br.com.mastertech.oAuthEx01.repositories;

import br.com.mastertech.oAuthEx01.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findByDono(int dono);
}
