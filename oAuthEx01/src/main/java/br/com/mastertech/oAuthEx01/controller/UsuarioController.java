package br.com.mastertech.oAuthEx01.controller;

import br.com.mastertech.oAuthEx01.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/usuario")
@RestController
public class UsuarioController {

    @GetMapping
    public Usuario pegarUsuario(@AuthenticationPrincipal Usuario usuario)
    {
        return usuario;
    }

}
