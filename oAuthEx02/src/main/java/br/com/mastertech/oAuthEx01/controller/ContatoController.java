package br.com.mastertech.oAuthEx01.controller;

import br.com.mastertech.oAuthEx01.model.Contato;
import br.com.mastertech.oAuthEx01.model.dto.ContatoMapeamento;
import br.com.mastertech.oAuthEx01.model.dto.RequisicaoNovoContato;
import br.com.mastertech.oAuthEx01.security.Usuario;
import br.com.mastertech.oAuthEx01.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/contato")
@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ContatoMapeamento contatoMapeamento;


    @PostMapping
    public Contato criarContato(@RequestBody RequisicaoNovoContato requisicaoNovoContato, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = contatoMapeamento.paraContato(requisicaoNovoContato);

        contato.setDono(usuario.getId());

        return contatoService.criarContato(contato);
    }

    @GetMapping
    public List<Contato> buscarContatos(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.buscarContatos(usuario.getId());
    }

}
