package br.com.mastertech.oAuthEx01.model.dto;

import br.com.mastertech.oAuthEx01.model.Contato;
import org.springframework.stereotype.Component;

@Component
public class ContatoMapeamento {

    public Contato paraContato(RequisicaoNovoContato requisicaoNovoContato)
    {
        Contato contato = new Contato();
        contato.setTelefone(requisicaoNovoContato.getTelefone());
        contato.setNome(requisicaoNovoContato.getNome());

        return contato;
    }

    public RequisicaoNovoContato paraRequisicaoNovoContato(Contato contato)
    {
        RequisicaoNovoContato requisicaoNovoContato = new RequisicaoNovoContato();
        requisicaoNovoContato.setNome(contato.getNome());
        requisicaoNovoContato.setTelefone(contato.getNome());

        return  requisicaoNovoContato;
    }

}
